# Find duplicate files. #

## Examples ##

Searching through someuser's home directory, quickly:
```
java -jar duupa-core-1.0.jar --path=/home/someuser --quick
```

Using a complete file comparison (slower) and including hidden files and directories:
```
java -jar duupa-core-1.0.jar --path=/home/someuser --includeHiddenFiles
```

## Options: ##

**--path= (required)**

Where to begin the search for duplicates.

example:
--path=/home/me/stuff

**--debug**

Print more information regarding what actions are occurring.

**--quick**

Only compare up to the first 1024 bytes of each file. For large files, this is much faster than comparing the entire file, but could be inaccurate. Don't take the results for granted.

**--quickSize=**

When using --quick, this specifies how many bytes from each file (maximum) to use for comparing files.

example:
--quickSize=65536

**--includeHiddenFiles**

By default, hidden files and directories are ignored. Set this option to include them in comparisons.

**--includeSymLinks**

By default, symbolic links are ignored. Set this option to follow them when traversing files.