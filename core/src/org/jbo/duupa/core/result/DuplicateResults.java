package org.jbo.duupa.core.result;


import org.jbo.duupa.core.duupa.RunOptions;
import org.jbo.duupa.core.unique.FileUniqueKey;

import java.nio.file.Path;
import java.util.*;

public class DuplicateResults {

    public DuplicateResults(RunOptions runOptions) {
        this.runOptions = runOptions;
    }

    public void add(FileUniqueKey key, Path file) {
        List<Path> paths = files.get(key);
        if (paths == null) {
            paths = new ArrayList<>();
            files.put(key, paths);
        }
        paths.add(file);
    }

    public void addAll(FileUniqueKey key, List<Path> files) {
        files.forEach(path ->
                add(key, path));
    }

    public void addAll(DuplicateResults duplicateResults) {
        duplicateResults.files.entrySet().forEach(entry ->
                addAll(entry.getKey(), entry.getValue()));
    }

    public DuplicateResults removeSingles() {
        files.entrySet().removeIf(entry -> entry.getValue().size() == 1);
        return this;
    }

    public List<Path> get(FileUniqueKey key) {
        return files.get(key);
    }

    public Collection<List<Path>> getAll() {
        return files.values();
    }

    public boolean hasResults() {
        return files.size() != 0;
    }

    public int size() {
        return files.size();
    }

    public RunOptions runOptions() {
        return runOptions;
    }

    private final Map<FileUniqueKey, List<Path>> files = new HashMap<>();
    private final RunOptions runOptions;
}
