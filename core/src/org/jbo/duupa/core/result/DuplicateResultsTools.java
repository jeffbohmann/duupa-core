package org.jbo.duupa.core.result;

import org.jbo.duupa.core.unique.FileUniqueKey;
import org.jbo.duupa.core.unique.FileUniqueKeyGenerator;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DuplicateResultsTools {

    public List<Path> getChildren(DuplicateResults results, Path parent) {
        List<Path> children = new ArrayList<>();
        if(parent == null || !parent.toFile().exists()) {
            return children;
        }

        results.getAll().forEach(resultPaths -> {
            resultPaths.forEach(resultPath -> {
                if(resultPath.getParent().equals(parent)) {
                    children.add(resultPath);
                }
            });
        });

        return children;
    }

    public Map<Path, List<Path>> getChildrenWithDuplicates(DuplicateResults results, Path parent) {
        Map<Path, List<Path>> children = new HashMap<>();
        this.getChildren(results, parent).forEach(child -> {
            FileUniqueKey key = new FileUniqueKeyGenerator().generate(child, results.runOptions().hashOptions());
            children.put(child, results.get(key));
        });

        return children;
    }

    public List<Path> getSiblings(DuplicateResults results, Path child) {
        List<Path> siblings = this.getChildren(results, child.getParent());
        siblings.remove(child);
        return siblings;
    }

    public List<Path> getDuplicates(DuplicateResults results, Path path) {
        return results.get(new FileUniqueKeyGenerator().generate(path, results.runOptions().hashOptions()));
    }
}
