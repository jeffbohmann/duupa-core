package org.jbo.duupa.core.hash;

import org.jbo.duupa.core.duupa.RunOptions;
import org.jbo.duupa.core.result.DuplicateResults;
import org.jbo.duupa.core.unique.FileUniqueKeyGenerator;
import org.jbo.duupa.core.utility.CallMe;
import org.jbo.log.Log;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;


public class Hasher {

    private static final int POOL_SIZE = 4;

    public void start(DuplicateResults toCheck, RunOptions options, CallMe<DuplicateResults> callMe) {
        DuplicateResults results = new DuplicateResults(options);

        AtomicLong countdown = new AtomicLong(toCheck.size());

        // go through each set of matches to make sure they hash the same
        for(List<Path> matchSet : toCheck.getAll()) {

            pool.execute(new HasherWorker(matchSet, options, hashedPaths -> {

                //add the results from the worker to the main results
                results.addAll(hashedPaths);

                //see if all the results are in
                if(countdown.decrementAndGet() == 0) {
                    callMe.done(results);
                }
            }));
        }
    }

    private final ExecutorService pool = Executors.newFixedThreadPool(POOL_SIZE);

    private static class HasherWorker implements Runnable {

        public HasherWorker(List<Path> hashesToCheck, RunOptions options, CallMe<DuplicateResults> callMe) {
            this.hashesToCheck = hashesToCheck;
            this.options = options;
            this.callMe = callMe;
        }

        @Override
        public void run() {
            DuplicateResults results = new DuplicateResults(options);
            for (Path path : hashesToCheck) {
                long start = System.currentTimeMillis();
                File file = path.toFile();

                results.add(new FileUniqueKeyGenerator().generate(path, options.hashOptions()), path);

                log.info("Checked hash for file[{}] ({}ms)", path, System.currentTimeMillis() - start);
            }
            callMe.done(results);
        }

        private List<Path> hashesToCheck;
        private RunOptions options;
        private CallMe<DuplicateResults> callMe;
        private static final Log log = Log.get();
    }
}
