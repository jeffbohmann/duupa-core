package org.jbo.duupa.core.hash;


public class HashOptions {

    private boolean quick;
    private long quickSize;

    public HashOptions quick(boolean quick) {
        this.quick = quick;
        return this;
    }

    public boolean quick() {
        return quick;
    }

    public HashOptions quickSize(long quickSize) {
        this.quickSize = quickSize;
        return this;
    }

    public long quickSize() {
        return quickSize;
    }
}
