package org.jbo.duupa.core;

import org.jbo.duupa.core.duupa.Duupa;
import org.jbo.duupa.core.duupa.RunOptions;
import org.jbo.duupa.core.result.DuplicateResults;
import org.jbo.log.Log;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 *
 */
public class Main {

    public static void main(String... args) {
        final Main main = new Main();
        main.start = System.currentTimeMillis();

        Path root = null;
        RunOptions options = new RunOptions();
        for(String arg : args) {
            if(arg.startsWith("--path")) {
                root = Paths.get(main.getArgValue(arg));
            }
            else if(arg.equals("--quick")) {
                options.hashOptions().quick(true);;
            }
            else if(arg.startsWith("--quickSize")) {
                options.hashOptions().quickSize(Long.valueOf(main.getArgValue(arg)));
            }
            else if(arg.equals("--includeHiddenFiles")) {
                options.includeHiddenFiles(true);
            }
            else if(arg.equals("--includeSymLinks")) {
                options.includeSymLinks(true);
            }
        }

        //check required args
        if(root == null || !root.toFile().exists()) {
            throw new IllegalArgumentException("Please provide a valid path argument.");
        }

        log.info("Searching files under[{}]...", root);
        new Duupa().start(root, options, result -> {
            main.print(result);
            System.exit(0);
        });
    }

    private void print(DuplicateResults matches) {
        boolean found = false;
        for(List<Path> matchSet : matches.getAll()) {
            found = true;
            log.info("-------------");
            log.info("Size: {}", matchSet.get(0).toFile().length());
            matchSet.forEach(path -> log.info(path.toString()));
            log.info("-------------");
        }

        long time = System.currentTimeMillis() - start;
        if(!found) {
            log.info("No duplicates found. ({}ms)", time);
        }
        else {
            log.info("Found {} duplications. ({}ms)", matches.size(), time);
        }
    }

    private String getArgValue(String arg) {
        return arg.substring(arg.indexOf("=") + 1, arg.length());
    }

    private long start;
    private static final Log log = Log.get();
}
