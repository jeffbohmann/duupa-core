package org.jbo.duupa.core.duupa;

import org.jbo.duupa.core.hash.HashOptions;

/**
 * Options that control how Duupa runs.
 */
public class RunOptions {

    private boolean includeHiddenFiles = false;
    private boolean includeSymLinks = false;
    private HashOptions hashOptions = new HashOptions();

    public RunOptions includeHiddenFiles(boolean includeHiddenFiles) {
        this.includeHiddenFiles = includeHiddenFiles;
        return this;
    }

    public boolean includeHiddenFiles() {
        return includeHiddenFiles;
    }

    public boolean includeSymLinks() {
        return includeSymLinks;
    }

    public RunOptions includeSymLinks(boolean includeSymLinks) {
        this.includeSymLinks = includeSymLinks;
        return this;
    }

    public HashOptions hashOptions() {
        return hashOptions;
    }

    public RunOptions hashOptions(HashOptions hashOptions) {
        this.hashOptions = hashOptions;
        return this;
    }
}
