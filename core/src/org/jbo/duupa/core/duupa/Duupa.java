package org.jbo.duupa.core.duupa;

import org.jbo.duupa.core.collect.FileCollector;
import org.jbo.duupa.core.collect.filter.ExcludeHiddenFiles;
import org.jbo.duupa.core.collect.filter.ExcludeSymLinks;
import org.jbo.duupa.core.collect.filter.Filter;
import org.jbo.duupa.core.hash.HashOptions;
import org.jbo.duupa.core.hash.Hasher;
import org.jbo.duupa.core.result.DuplicateResults;
import org.jbo.duupa.core.unique.FileUniqueKeyGenerator;
import org.jbo.duupa.core.utility.CallMe;
import org.jbo.log.Log;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Main entry point for using Duupa.
 */
public class Duupa {

    public void start(Path root,
                      RunOptions options,
                      CallMe<DuplicateResults> callback) {

        List<Filter> filters = new ArrayList<>();
        if(!options.includeHiddenFiles()) {
            filters.add(new ExcludeHiddenFiles());
        }
        if(!options.includeSymLinks()) {
            filters.add(new ExcludeSymLinks());
        }

        DuplicateResults result = new FileCollector().collect(root, filters, new FileUniqueKeyGenerator(), options);

        if (result.hasResults()) {
            //run a simple hash check
            log.info("Performing checksum for " + result.size() + " sets of files...");

            //first do a quick hash check to trim down the results
            HashOptions quickHasherOptions = new HashOptions().quick(true);
            new Hasher().start(result,
                    new RunOptions()
                            .includeHiddenFiles(options.includeHiddenFiles())
                            .includeSymLinks(options.includeSymLinks())
                            //TODO: is this a good private default size or should this become a preference (passed in)?
                            .hashOptions(new HashOptions().quick(true).quickSize(1024)),
                    quickResults -> {

                //run a more in-depth check if specified (and needed)
                if (quickResults.hasResults() && !options.hashOptions().quick()) {
                    //continue with a full hash check
                    new Hasher().start(quickResults, options, finalResults -> {
                        callback.done(finalResults.removeSingles());
                    });
                }
                else {
                    //done, already did the quick check
                    callback.done(quickResults.removeSingles());
                }
            });
        }
    }

    private static final Log log = Log.get();
}
