package org.jbo.duupa.core.collect.filter;

import org.jbo.log.Log;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


public class ExcludeHiddenFiles implements Filter {

    @Override
    public boolean exclude(Path path) {
        try {
            return Files.isHidden(path);
        }
        catch(IOException e) {
            log.error("Error checking if file is hidden.", e);
            return false;
        }
    }
    private static final Log log = Log.get();
}
