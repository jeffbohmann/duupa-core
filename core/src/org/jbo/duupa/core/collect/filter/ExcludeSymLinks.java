package org.jbo.duupa.core.collect.filter;


import java.nio.file.Files;
import java.nio.file.Path;

public class ExcludeSymLinks implements Filter {

    @Override
    public boolean exclude(Path path) {
        return Files.isSymbolicLink(path);
    }
}
