package org.jbo.duupa.core.collect.filter;

import java.nio.file.Path;


public interface Filter {
    boolean exclude(Path path);
}
