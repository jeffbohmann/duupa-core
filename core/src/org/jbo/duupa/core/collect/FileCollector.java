package org.jbo.duupa.core.collect;

import org.jbo.duupa.core.collect.filter.Filter;
import org.jbo.duupa.core.duupa.RunOptions;
import org.jbo.duupa.core.result.DuplicateResults;
import org.jbo.duupa.core.unique.FileUniqueKeyGenerator;
import org.jbo.log.Log;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;


public class FileCollector {

    public DuplicateResults collect(final Path root,
                                    final List<Filter> filters,
                                    final FileUniqueKeyGenerator generator,
                                    final RunOptions runOptions) {

        final DuplicateResults result = new DuplicateResults(runOptions);

        try {
            Files.walkFileTree(root, new FileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

                    for (Filter filter : filters) {
                        if (filter.exclude(dir)) {
                            return FileVisitResult.SKIP_SUBTREE;
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

                    for (Filter filter : filters) {
                        if (filter.exclude(file)) {
                            return FileVisitResult.CONTINUE;
                        }
                    }

                    result.add(generator.generate(file, runOptions.hashOptions()), file);

                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException e) throws IOException {
                    log.warn("Error checking file[" + file + "]. Continuing...", e);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });

            return result;
        }
        catch(IOException e) {
            log.error("Error while traversing files.", e);
            throw new RuntimeException(e);
        }
    }
    private static final Log log = Log.get();
}