package org.jbo.duupa.core.utility;


import java.io.File;

public class FileHelper {
    public String extension(File file) {
        String filename = file.getName();
        return filename.substring(filename.lastIndexOf(".") + 1, filename.length()).toLowerCase();
    }
}
