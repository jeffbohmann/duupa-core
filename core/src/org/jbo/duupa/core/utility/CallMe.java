package org.jbo.duupa.core.utility;


public interface CallMe<T> {
    void done(T result);
}
