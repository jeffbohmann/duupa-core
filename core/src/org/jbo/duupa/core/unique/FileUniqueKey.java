package org.jbo.duupa.core.unique;


import org.jbo.duupa.core.hash.HashOptions;

public class FileUniqueKey {

    public FileUniqueKey(String extension, Long size, Long checksum, HashOptions hashOptions) {
        this.extension = extension;
        this.size = size;
        this.checksum = checksum;
        this.hashOptions = hashOptions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileUniqueKey)) return false;

        FileUniqueKey that = (FileUniqueKey) o;

        if (checksum != null ? !checksum.equals(that.checksum) : that.checksum != null) return false;
        if (!size.equals(that.size)) return false;
        return !(extension != null ? !extension.equals(that.extension) : that.extension != null);
    }

    @Override
    public int hashCode() {
        int result = checksum != null ? checksum.hashCode() : 0;
        result = 31 * result + size.hashCode();
        result = 31 * result + (extension != null ? extension.hashCode() : 0);
        return result;
    }

    public Long getChecksum() {
        return checksum;
    }

    public Long getSize() {
        return size;
    }

    public String getExtension() {
        return extension;
    }

    public HashOptions getHashOptions() {
        return hashOptions;
    }

    private Long checksum;
    private Long size;
    private String extension;
    private HashOptions hashOptions;
}
