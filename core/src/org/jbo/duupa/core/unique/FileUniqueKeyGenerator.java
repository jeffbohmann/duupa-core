package org.jbo.duupa.core.unique;

import net.jpountz.xxhash.StreamingXXHash64;
import net.jpountz.xxhash.XXHashFactory;
import org.jbo.duupa.core.hash.HashOptions;
import org.jbo.duupa.core.utility.FileHelper;
import org.jbo.log.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;


public class FileUniqueKeyGenerator {

    public FileUniqueKey generate(Path path, HashOptions hashOptions) {
        File file = path.toFile();
        String extension = new FileHelper().extension(file);
        long size = file.length();

        //TODO: since this is a streaming hasher, should this loop over the file contents, calling update
        //TODO:   for each chunk up to the keyLength valid (should be a long, or maybe even larger?)
        int bytesToRead = hashOptions.quick() ? new Long(hashOptions.quickSize()).intValue() : new Long(size).intValue();

        byte[] data = new byte[bytesToRead];
        int lengthRead;
        try(FileInputStream stream = new FileInputStream(file)) {
            lengthRead = stream.read(data);
        }
        catch (FileNotFoundException e) {
            String msg = "Read error while hashing file[{}]";
            log.error(e, msg, file);
            throw new RuntimeException(msg.replace("{}", file.toString()), e);
        }
        catch (IOException e) {
            String msg = "I/O error while hashing file[{}]";
            log.error(e, msg, file);
            throw new RuntimeException(msg.replace("{}", file.toString()), e);
        }

        //set up and perform hashing
        StreamingXXHash64 hasher = XXHashFactory.fastestInstance().newStreamingHash64(0);
        hasher.update(data, 0, lengthRead);
        return new FileUniqueKey(extension, size, hasher.getValue(), hashOptions);
    }

    private static final Log log = Log.get();
}
